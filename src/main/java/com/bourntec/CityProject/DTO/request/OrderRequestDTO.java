package com.bourntec.CityProject.DTO.request;




import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;

import org.springframework.beans.BeanUtils;

import com.bourntec.CityProject.model.Order;
import com.bourntec.CityProject.model.ModeOfPayment;
import com.bourntec.CityProject.model.Status;

import lombok.Data;

@Data
public class OrderRequestDTO {

	private Integer id;
	@NotNull(message="productId cannot be empty")
	private Integer productId;
	@NotNull(message="customerId cannot be empty")
    private Integer customerId;
	@NotNull(message="deliveryBoyId cannot be empty")
	private Integer deliveryBoyId;
	
    private ModeOfPayment mode;
	private Status status;
	@Email
	String email;

	
 public Order ConvertToModel()
 { 
	 Order order=new Order();
 BeanUtils.copyProperties(this, order);
 return order;
 }
	
}
