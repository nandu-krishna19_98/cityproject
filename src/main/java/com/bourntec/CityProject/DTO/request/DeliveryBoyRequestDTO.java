package com.bourntec.CityProject.DTO.request;

import java.time.LocalDate;


import javax.validation.constraints.Email;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.beans.BeanUtils;

import com.bourntec.CityProject.model.DeliveryBoy;

import lombok.Data;

@Data
public class DeliveryBoyRequestDTO {
	@NotNull(message="Name cannot be empty")
	@Size(min=4,max=30)
	 private String name;
	@NotNull(message="productId cannot be empty")
	 private Integer productId;
	@NotNull(message="customerId cannot be empty")
	 private Integer customerId;
	 @NotNull
	 @Min(10)
	 @Max(15)
	 private Integer phoneNumber;
	
	 private LocalDate timeToDeliver;
	 
		@Email
		String email;

		
	 public DeliveryBoy ConvertToModel()
	 { 
		 DeliveryBoy deliveryBoy=new DeliveryBoy();
	 BeanUtils.copyProperties(this, deliveryBoy);
	 return deliveryBoy;
	 }
}
