package com.bourntec.CityProject.DTO.request;

import java.time.LocalDate;


import javax.validation.constraints.Email;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.beans.BeanUtils;

import com.bourntec.CityProject.model.Customer;

import lombok.Data;
@Data
public class CustomerRequestDTO {
	@NotNull(message="Name cannot be empty")
	@Size(min=4,max=30)
	private String name;
	@NotNull(message="customerId cannot be empty")
	 private Integer customerId;
	@NotNull(message="address cannot be empty")
	 private String address;
	 @NotNull
	 @Min(10)
	 @Max(15)
	 private Integer phoneNumber;
	 @NotNull(message="noOfProductPurchased cannot be empty")
	 private Integer noOfProductPurchased;
	 @NotNull(message="quantityOfProduct cannot be empty")
	 private Double quantityOfProduct;
	 private LocalDate receivedTime;
	 
		@Email
		String email;

		
	 public Customer ConvertToModel()
	 { 
	 Customer customer=new Customer();
	 BeanUtils.copyProperties(this, customer);
	 return customer;
	 }

}
