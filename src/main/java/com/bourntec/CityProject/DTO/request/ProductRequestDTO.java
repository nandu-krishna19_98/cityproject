package com.bourntec.CityProject.DTO.request;

import java.time.LocalDate;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.beans.BeanUtils;

import com.bourntec.CityProject.model.Product;

import lombok.Data;

@Data
public class ProductRequestDTO {
	@NotNull(message="product_id cannot be empty")
	private Integer product_id;
	@NotNull(message="Name cannot be empty")
	@Size(min=4,max=30)
    private String productName;
	@NotNull(message="quantity cannot be empty")
    private Integer quantity;
	@NotNull(message="price cannot be empty")
    private Double price;
	@NotNull(message="expireField cannot be empty")
    private String expireField;
 
    private LocalDate manufacDate;
    private LocalDate expiryDate;
    
    public Product ConvertToModel()
    { 
    Product product=new Product();
    BeanUtils.copyProperties(this, product);
    return product;
    }
    
}
