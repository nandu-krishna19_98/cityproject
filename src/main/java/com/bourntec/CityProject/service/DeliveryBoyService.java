package com.bourntec.CityProject.service;

import java.util.List;

import com.bourntec.CityProject.DTO.request.CustomerRequestDTO;
import com.bourntec.CityProject.DTO.request.DeliveryBoyRequestDTO;
import com.bourntec.CityProject.model.DeliveryBoy;
import com.bourntec.CityProject.responseDTO.CustomerResponseDTO;
import com.bourntec.CityProject.responseDTO.DeliveryBoyResponseDTO;

public interface DeliveryBoyService {

	
	public List<DeliveryBoyResponseDTO> getAll();
	 List<DeliveryBoy>findByNameAndRecordStatus(String name,String recordStatus);
	public  DeliveryBoy findById(int id);
	public String  deleteById(int id);
	public DeliveryBoyResponseDTO save(DeliveryBoyRequestDTO DeliveryBoyRequestDTO);
	public List<DeliveryBoyResponseDTO> saveAll(List<DeliveryBoyRequestDTO> deliveryBoyServiceRequestDTOList);
	//public DeliveryBoyResponseDTO update(int id,DeliveryBoyRequestDTO DeliveryBoyRequestDTO);
	public DeliveryBoy update(int id, DeliveryBoy deliveryBoy);
}
