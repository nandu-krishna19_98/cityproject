package com.bourntec.CityProject.service;

import java.util.List;

import com.bourntec.CityProject.DTO.request.DeliveryBoyRequestDTO;
import com.bourntec.CityProject.DTO.request.OrderRequestDTO;
import com.bourntec.CityProject.model.Order;
import com.bourntec.CityProject.responseDTO.DeliveryBoyResponseDTO;
import com.bourntec.CityProject.responseDTO. OrderResponseDTO;

public interface OrderService {

	List<OrderResponseDTO> getAll();

	public  Order findById(int id);
	
	public String  deleteById(int id);
	
	public OrderResponseDTO save(OrderRequestDTO orderRequestDTO);

	 List<OrderResponseDTO> saveAll(List<OrderRequestDTO> orderServiceRequestDTOList);
	
	// OrderResponseDTO update(int id,OrderRequestDTO orderRequestDTO);

	Order update(int id, Order order);

	
}
