package com.bourntec.CityProject.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;

import com.bourntec.CityProject.DTO.request.CustomerRequestDTO;
import com.bourntec.CityProject.DTO.request.DeliveryBoyRequestDTO;
import com.bourntec.CityProject.exception.FileErrorException;
import com.bourntec.CityProject.exception.RecordNotFoundException;
import com.bourntec.CityProject.model.Customer;
import com.bourntec.CityProject.model.DeliveryBoy;
import com.bourntec.CityProject.repository.DeliveryBoyRepository;
import com.bourntec.CityProject.responseDTO.CustomerResponseDTO;
import com.bourntec.CityProject.responseDTO.DeliveryBoyResponseDTO;
import com.bourntec.CityProject.util.Constants;

public class DeliveryBoyServiceImpl {

	@Autowired
	DeliveryBoyRepository deliveryBoyRepository;
	
	/*public List<DeliveryBoyResponseDTO> getAll() {
		return deliveryBoyRepository.findByRecordStatus(Constants.ACTIVE).stream()
				.map(DeliveryBoyResponseDTO::new)
				.collect(Collectors.toList());
	}*/
	
	public List<DeliveryBoy>findByNameAndRecordStatus(String name,String recordStatus)
	{
	return deliveryBoyRepository.findByNameAndRecordStatus(name,recordStatus);
	}
	
	public  DeliveryBoy findById(int id) throws RecordNotFoundException
	{
			Optional<DeliveryBoy> deliveryBoyOptional=deliveryBoyRepository.findById(id);
			
			if(deliveryBoyOptional.isPresent())
				return deliveryBoyOptional.get();
			else
				throw new RecordNotFoundException("Record not found");
	}
	
	public String deleteById(int id)
	{
		
		 if(deliveryBoyRepository.existsById(id)==true)
	        {
			 deliveryBoyRepository.deleteById(id);
	            return "Record Deleted";
	        }
	       else
	            return "record not exist";
	        
	}
	
	public DeliveryBoyResponseDTO save(DeliveryBoyRequestDTO deliveryBoyRequestDTO)
	{
		
		DeliveryBoy deliveryBoy = deliveryBoyRequestDTO.ConvertToModel();
		deliveryBoy.setRecordStatus(Constants.ACTIVE);
		deliveryBoy = deliveryBoyRepository.save(deliveryBoy);
		return new DeliveryBoyResponseDTO(deliveryBoy);
	
	  }

	public List<DeliveryBoyResponseDTO> saveAll(List<DeliveryBoyRequestDTO> deliveryBoyRequestDTOList) 
	{
		
		 		
			    List<DeliveryBoy> deliveryBoyList=new ArrayList<>();
			    DeliveryBoy deliveryBoy=null;
			     for (DeliveryBoyRequestDTO deliveryBoyRequestDTO : deliveryBoyRequestDTOList) {
			    	 deliveryBoy=deliveryBoyRequestDTO.ConvertToModel();
			    	 deliveryBoy.setRecordStatus(Constants.ACTIVE);
			    	 deliveryBoyList.add(deliveryBoy);
			    	
			    	
			    }
			    return deliveryBoyRepository.saveAll(deliveryBoyList).
			    		stream()
			    		.map(DeliveryBoyResponseDTO::new).toList();    
			    
			    
	}
	
	public DeliveryBoy update(int id,DeliveryBoy deliveryBoy) throws FileErrorException
	{

	Optional<DeliveryBoy> deliveryBoyOptional=deliveryBoyRepository.findById(id);
	if(deliveryBoyOptional.isPresent()) {
		DeliveryBoy existingDeliveryBoy=deliveryBoyOptional.get();
		deliveryBoy.setCreatedDate(existingDeliveryBoy.getCreatedDate());
		deliveryBoy.setId(id);
	return deliveryBoyRepository.save(deliveryBoy);
	}
	else

	throw new FileErrorException(null);


	}
	
    

}
