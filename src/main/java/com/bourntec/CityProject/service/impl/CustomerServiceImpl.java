package com.bourntec.CityProject.service.impl;

import java.util.ArrayList;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import com.bourntec.CityProject.DTO.request.CustomerRequestDTO;
import com.bourntec.CityProject.exception.FileErrorException;
import com.bourntec.CityProject.exception.RecordNotFoundException;
import com.bourntec.CityProject.model.Customer;
import com.bourntec.CityProject.repository.CustomerRepository;
import com.bourntec.CityProject.responseDTO.CustomerResponseDTO;
import com.bourntec.CityProject.util.Constants;








public class CustomerServiceImpl {
	

	@Autowired
	CustomerRepository customerRepository;
	
	/*public List<CustomerResponseDTO> getAll() {
			return customerRepository.findByRecordStatus(Constants.ACTIVE).stream()
					.map(CustomerResponseDTO::new)
					.collect(Collectors.toList());
		}*/
	public List<Customer>findByNameAndRecordStatus(String name,String recordStatus)
	{
	return customerRepository.findByNameAndRecordStatus(name,recordStatus);
	}
	
	public List<Customer>findByAddressAndRecordStatus(String address,String recordStatus)
	{
	return customerRepository.findByAddressAndRecordStatus(address,recordStatus);
	}
	public  Customer findById(int id) throws RecordNotFoundException
	{
			Optional<Customer> customerOptional=customerRepository.findById(id);
			
			if(customerOptional.isPresent())
				return customerOptional.get();
			else
				throw new RecordNotFoundException("Record not found");
	}
	
	public String deleteById(int id)
	{
		
		 if(customerRepository.existsById(id)==true)
	        {
			 customerRepository.deleteById(id);
	            return "Record Deleted";
	        }
	       else
	            return "record not exist";
	        
	}
	
	public CustomerResponseDTO save(CustomerRequestDTO customerRequestDTO)
	{
		
		Customer customer = customerRequestDTO.ConvertToModel();
		customer.setRecordStatus(Constants.ACTIVE);
		customer = customerRepository.save(customer);
		return new CustomerResponseDTO(customer);
	
	  }
	
	 
	public List<CustomerResponseDTO> saveAll(List<CustomerRequestDTO> customerRequestDTOList) 
	{
		
		 		
			    List<Customer> customerList=new ArrayList<>();
			    Customer customer=null;
			     for (CustomerRequestDTO customerRequestDTO : customerRequestDTOList) {
			    	 customer=customerRequestDTO.ConvertToModel();
			    	 customer.setRecordStatus(Constants.ACTIVE);
			    	 customerList.add(customer);
			    	
			    	
			    }
			    return customerRepository.saveAll(customerList).
			    		stream()
			    		.map(CustomerResponseDTO::new).toList();    
			    
			    
	}
	
	public Customer update(int id,Customer customer) throws FileErrorException
	{

	Optional<Customer> customerOptional=customerRepository.findById(id);
	if(customerOptional.isPresent()) {
		Customer existingCustomer=customerOptional.get();
		customer.setCreatedDate(existingCustomer.getCreatedDate());
		customer.setId(id);
	return customerRepository.save(customer);
	}
	else

	throw new FileErrorException(null);


	}
	
    }
