package com.bourntec.CityProject.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;

import com.bourntec.CityProject.DTO.request.DeliveryBoyRequestDTO;
import com.bourntec.CityProject.DTO.request.OrderRequestDTO;
import com.bourntec.CityProject.exception.FileErrorException;
import com.bourntec.CityProject.exception.RecordNotFoundException;
import com.bourntec.CityProject.model.DeliveryBoy;
import com.bourntec.CityProject.model.Order;
import com.bourntec.CityProject.repository.OrderRepository;
import com.bourntec.CityProject.responseDTO.DeliveryBoyResponseDTO;
import com.bourntec.CityProject.responseDTO.OrderResponseDTO;
import com.bourntec.CityProject.util.Constants;

public class OrderServiceImpl {
	@Autowired
	OrderRepository orderRepository;

	/*public List<OrderResponseDTO> getAll() {
		return orderRepository.findByRecordStatus(Constants.ACTIVE).stream()
				.map(OrderResponseDTO::new)
				.collect(Collectors.toList());
	}*/
	
	public  Order findById(int id) throws RecordNotFoundException
	{
			Optional<Order> orderOptional=orderRepository.findById(id);
			
			if(orderOptional.isPresent())
				return orderOptional.get();
			else
				throw new RecordNotFoundException("Record not found");
	}
	
	public String deleteById(int id)
	{
		
		 if(orderRepository.existsById(id)==true)
	        {
			 orderRepository.deleteById(id);
	            return "Record Deleted";
	        }
	       else
	            return "record not exist";
	        
	}
	
	public OrderResponseDTO save(OrderRequestDTO orderRequestDTO)
	{
		
		Order order = orderRequestDTO.ConvertToModel();
		order.setRecordStatus(Constants.ACTIVE);
		order = orderRepository.save(order);
		return new OrderResponseDTO(order);
	
	  }
	
	public List<OrderResponseDTO> saveAll(List<OrderRequestDTO> orderRequestDTOList) 
	{
		
		 		
			    List<Order> orderList=new ArrayList<>();
			    Order order=null;
			     for (OrderRequestDTO orderRequestDTO : orderRequestDTOList) {
			    	 order=orderRequestDTO.ConvertToModel();
			    	 order.setRecordStatus(Constants.ACTIVE);
			    	 orderList.add(order);
			    	
			    	
			    }
			    return orderRepository.saveAll(orderList).
			    		stream()
			    		.map(OrderResponseDTO::new).toList();    
			    
			    
	}
	
	public Order update(int id,Order order) throws FileErrorException
	{

	Optional<Order> orderOptional=orderRepository.findById(id);
	if(orderOptional.isPresent()) {
		Order existingOrder=orderOptional.get();
		order.setCreatedDate(existingOrder.getCreatedDate());
		order.setId(id);
	return orderRepository.save(order);
	}
	else

	throw new FileErrorException(null);


	}
	

}
