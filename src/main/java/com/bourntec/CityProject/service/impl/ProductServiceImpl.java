package com.bourntec.CityProject.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;

import com.bourntec.CityProject.DTO.request.OrderRequestDTO;
import com.bourntec.CityProject.DTO.request.ProductRequestDTO;
import com.bourntec.CityProject.exception.FileErrorException;
import com.bourntec.CityProject.exception.RecordNotFoundException;
import com.bourntec.CityProject.model.Order;
import com.bourntec.CityProject.model.Product;
import com.bourntec.CityProject.repository.ProductRepository;
import com.bourntec.CityProject.responseDTO.OrderResponseDTO;
import com.bourntec.CityProject.responseDTO.ProductResponseDTO;
import com.bourntec.CityProject.util.Constants;

public class ProductServiceImpl {
	@Autowired
	ProductRepository productRepository;
	
/*	public List<ProductResponseDTO> getAll() {
		return productRepository.findByRecordStatus(Constants.ACTIVE).stream()
				.map(ProductResponseDTO::new)
				.collect(Collectors.toList());
	}*/
	
	public List<Product>findByNameAndRecordStatus(String name,String recordStatus)
	{
	return productRepository.findByNameAndRecordStatus(name,recordStatus);
	}
	
	public List<Product>findByProductNameAndRecordStatus(String productName,String recordStatus)
	{
	return productRepository.findByProductNameAndRecordStatus(productName,recordStatus);
	}
	
	public List<Product>findByExpireFieldAndRecordStatus(String expireField,String recordStatus)
	{
	return productRepository.findByExpireFieldAndRecordStatus(expireField,recordStatus);
	}
	
	public  Product findById(int id) throws RecordNotFoundException
	{
			Optional<Product> productOptional=productRepository.findById(id);
			
			if(productOptional.isPresent())
				return productOptional.get();
			else
				throw new RecordNotFoundException("Record not found");
	}
	
	public String deleteById(int id)
	{
		
		 if(productRepository.existsById(id)==true)
	        {
			 productRepository.deleteById(id);
	            return "Record Deleted";
	        }
	       else
	            return "record not exist";
	        
	}
	
	public ProductResponseDTO save(ProductRequestDTO productRequestDTO)
	{
		
		Product product = productRequestDTO.ConvertToModel();
		product.setRecordStatus(Constants.ACTIVE);
		product = productRepository.save(product);
		return new ProductResponseDTO(product);
	
	  }
	
	public List<ProductResponseDTO> saveAll(List<ProductRequestDTO> productRequestDTOList) 
	{
		
		 		
			    List<Product> productList=new ArrayList<>();
			    Product product=null;
			     for (ProductRequestDTO productRequestDTO : productRequestDTOList) {
			    	 product=productRequestDTO.ConvertToModel();
			    	 product.setRecordStatus(Constants.ACTIVE);
			    	 productList.add(product);
			    	
			    	
			    }
			    return productRepository.saveAll(productList).
			    		stream()
			    		.map(ProductResponseDTO::new).toList();    
			    
			    
	}
	
	public Product update(int id,Product product) throws FileErrorException
	{

	Optional<Product> productOptional=productRepository.findById(id);
	if(productOptional.isPresent()) {
		Product existingProduct=productOptional.get();
		product.setCreatedDate(existingProduct.getCreatedDate());
		product.setId(id);
	return productRepository.save(product);
	}
	else

	throw new FileErrorException(null);


	}
}
