package com.bourntec.CityProject.service;

import java.util.List;

import com.bourntec.CityProject.DTO.request.OrderRequestDTO;
import com.bourntec.CityProject.DTO.request.ProductRequestDTO;
import com.bourntec.CityProject.model.Product;
import com.bourntec.CityProject.responseDTO.OrderResponseDTO;
import com.bourntec.CityProject.responseDTO.ProductResponseDTO;

public interface ProductService {


public List<Product>findByNameAndRecordStatus(String name,String recordStatus);

public List<Product> findByProductNameAndRecordStatus(String productName,String recordStatus);

public List<Product> findByExpireFieldAndRecordStatus(String expireField,String recordStatus);

public List<ProductResponseDTO> getAll();

public  Product findById(int id);

public String  deleteById(int id);

public ProductResponseDTO save(ProductRequestDTO productRequestDTO);

public List<ProductResponseDTO> saveAll(List<ProductRequestDTO> productServiceRequestDTOList);

//ProductResponseDTO update(int id,ProductRequestDTO productRequestDTO);

public Product update(int id, Product product);
}
