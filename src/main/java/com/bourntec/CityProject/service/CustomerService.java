package com.bourntec.CityProject.service;

import java.util.List;


import com.bourntec.CityProject.DTO.request.CustomerRequestDTO;
import com.bourntec.CityProject.model.Customer;
import com.bourntec.CityProject.responseDTO.CustomerResponseDTO;




public interface CustomerService {

	
	public List<Customer>findByNameAndRecordStatus(String name,String recordStatus);
	public List<Customer>findByAddressAndRecordStatus(String address,String recordStatus);
	public List<CustomerResponseDTO> getAll();
	public  Customer findById(int id);
	public String  deleteById(int id);
	public CustomerResponseDTO save(CustomerRequestDTO customerRequestDTO);
	public List<CustomerResponseDTO> saveAll(List<CustomerRequestDTO> customerRequestDTOList);
	//public CustomerResponseDTO update(int id,CustomerRequestDTO customerRequestDTO);
	public Customer update(int id, Customer customer);
}
