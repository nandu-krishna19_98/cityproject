package com.bourntec.CityProject.model;

import java.time.LocalDate;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import lombok.Data;

@Entity
@Table(name="product")
@EntityListeners(AuditingEntityListener.class)
@Data
public class Product{
    @Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)	
    private Integer id;
	private Integer product_id;
    private String productName;
    private Integer quantity;
    private Double price;
    private String expireField;
 
    private LocalDate manufacDate;
    private LocalDate expiryDate;
    @Column (length=1)
	private String recordStatus;
    @CreatedDate
   	LocalDateTime createdDate;
   	@LastModifiedDate
   	LocalDateTime lastModifiedDate;
 
}
