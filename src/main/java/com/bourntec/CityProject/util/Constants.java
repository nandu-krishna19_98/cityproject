package com.bourntec.CityProject.util;

public class Constants {

	public static final String SUCCESS_FOLDER="Success";
	public static final String FAILURE_FOLDER="Failure";
	public static final String FOLDER_SEPERATOR="\\";
	public static final String CSV_FILE_EXTENSION=".csv";
	public static final String JSON_FILE_EXTENSION=".json";
	public static final String EXCEL_FILE_EXTENSION=".xlsx";
	public static final String CSV_FOLDER="csv";
	public static final String JSON_FOLDER="json";
	public static final String ACTIVE="A";
	public static final String DELETED="D";
	
}
