package com.bourntec.CityProject.responseDTO;

import java.time.LocalDate;

import org.springframework.beans.BeanUtils;

import com.bourntec.CityProject.model.Customer;

import lombok.Data;


@Data
public class CustomerResponseDTO {
	
	 private String name;
	 private Integer customerId;
	 private String address;
	 private Integer phoneNumber;
	 private Integer noOfProductPurchased;
	 private Double quantityOfProduct;
	 private LocalDate receivedTime;
	 
	 public CustomerResponseDTO(Customer customer)
	 {
		 BeanUtils.copyProperties(this, customer);
}
}
