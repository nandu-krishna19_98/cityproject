package com.bourntec.CityProject.responseDTO;

import java.time.LocalDate;

import org.springframework.beans.BeanUtils;


import com.bourntec.CityProject.model.ModeOfPayment;
import com.bourntec.CityProject.model.Order;
import com.bourntec.CityProject.model.Status;

import lombok.Data;
@Data
public class OrderResponseDTO {

	private Integer id;
	private Integer productId;
    private Integer customerId;
	private Integer deliveryBoyId;
	
    private ModeOfPayment mode;
	private Status status;
	
	public OrderResponseDTO(Order order)
	 {
		 BeanUtils.copyProperties(this, order);
}
}
