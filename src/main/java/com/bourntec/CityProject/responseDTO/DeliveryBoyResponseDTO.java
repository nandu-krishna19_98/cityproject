package com.bourntec.CityProject.responseDTO;

import java.time.LocalDate;

import org.springframework.beans.BeanUtils;

import com.bourntec.CityProject.model.DeliveryBoy;

import lombok.Data;
@Data
public class DeliveryBoyResponseDTO {

	 private String name;
	 private Integer productId;
	 private Integer customerId;
	 private Integer phoneNumber;
	
	 private LocalDate timeToDeliver;
	 
	 public DeliveryBoyResponseDTO(DeliveryBoy deliveryBoy)
	 {
		 BeanUtils.copyProperties(this, deliveryBoy);
}
}
