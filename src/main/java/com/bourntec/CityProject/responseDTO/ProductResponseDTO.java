package com.bourntec.CityProject.responseDTO;

import java.time.LocalDate;

import org.springframework.beans.BeanUtils;

import com.bourntec.CityProject.model.Product;

import lombok.Data;
@Data
public class ProductResponseDTO {

	private Integer product_id;
    private String productName;
    private Integer quantity;
    private Double price;
    private String expireField;
 
    private LocalDate manufacDate;
    private LocalDate expiryDate;
    
    public ProductResponseDTO(Product product)
	 {
		 BeanUtils.copyProperties(this, product);
}
}
