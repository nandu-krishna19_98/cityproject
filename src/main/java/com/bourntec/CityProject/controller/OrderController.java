package com.bourntec.CityProject.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.bourntec.CityProject.DTO.request.DeliveryBoyRequestDTO;
import com.bourntec.CityProject.DTO.request.OrderRequestDTO;
import com.bourntec.CityProject.exception.FileErrorException;
import com.bourntec.CityProject.model.DeliveryBoy;
import com.bourntec.CityProject.model.Order;
import com.bourntec.CityProject.responseDTO.DeliveryBoyResponseDTO;
import com.bourntec.CityProject.responseDTO.OrderResponseDTO;
import com.bourntec.CityProject.service.OrderService;



public class OrderController {
	@Autowired
	OrderService orderService;
	
	@GetMapping("/getall")
	public List<OrderResponseDTO> getAll()
	{
		return orderService.getAll();
	}
	
	@GetMapping("/{id}")
	 public Order get(@PathVariable int id)
	 {
		 return orderService.findById(id);
	 }
	
	@DeleteMapping("/{id}")
	 public void delete(@PathVariable int id)
	 {
		orderService.deleteById(id);
	 }
	
	/* @PostMapping("/create-new")
	 public OrderResponseDTO create(@RequestBody @Valid OrderRequestDTO orderRequestDTO)
	 {
	 return orderService.save(orderRequestDTO);
	 }*/
	
	@PostMapping("/create")
	public ResponseEntity create(@RequestBody @Valid OrderRequestDTO orderRequestDTO)
	{
		return ResponseEntity.status(HttpStatus.CREATED).body(orderService.save(orderRequestDTO));
	}
	
	@PostMapping("/bulk")
	public List<OrderResponseDTO> createAll(@RequestBody List<OrderRequestDTO> orderServiceRequestDTOList)
	{
	return orderService.saveAll(orderServiceRequestDTOList);
	}
	
	 @PutMapping(value = "{id}")
		public Order update(@PathVariable int id, @RequestBody Order order) throws FileErrorException{
		return orderService.update(id,order);
		}
	 

}
