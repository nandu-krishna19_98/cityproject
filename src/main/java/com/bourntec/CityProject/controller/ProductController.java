package com.bourntec.CityProject.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.bourntec.CityProject.DTO.request.OrderRequestDTO;
import com.bourntec.CityProject.DTO.request.ProductRequestDTO;
import com.bourntec.CityProject.exception.FileErrorException;
import com.bourntec.CityProject.model.Order;
import com.bourntec.CityProject.model.Product;
import com.bourntec.CityProject.responseDTO.OrderResponseDTO;
import com.bourntec.CityProject.responseDTO.ProductResponseDTO;
import com.bourntec.CityProject.service.ProductService;



public class ProductController {
	@Autowired
	ProductService productService;
	

	@GetMapping("/getall")
	public List<ProductResponseDTO> getAll()
	{
		return productService.getAll();
	}
	@GetMapping("/getName")
	public List<Product>findByNameAndRecordStatus(String name,String recordStatus)
	{
	return productService.findByNameAndRecordStatus(name,recordStatus);
	}
	@GetMapping("/getProductName")
	public List<Product>findByProductNameAndRecordStatus(String productName,String recordStatus)
	{
	return productService.findByProductNameAndRecordStatus(productName,recordStatus);
	}
	
	@GetMapping("/getExpireField")
	public List<Product>findByExpireFieldAndRecordStatus(String expireField,String recordStatus)
	{
	return productService.findByExpireFieldAndRecordStatus(expireField,recordStatus);
	}
	
	@GetMapping("/{id}")
	 public Product get(@PathVariable int id)
	 {
		 return productService.findById(id);
	 }
	
	@DeleteMapping("/{id}")
	 public void delete(@PathVariable int id)
	 {
		productService.deleteById(id);
	 }
	
	/* @PostMapping("/create-new")
	 public ProductResponseDTO create(@RequestBody @Valid ProductRequestDTO productRequestDTO)
	 {
	 return productService.save(productRequestDTO);
	 }*/
	
	@PostMapping("/create")
	public ResponseEntity create(@RequestBody @Valid ProductRequestDTO productRequestDTO)
	{
		return ResponseEntity.status(HttpStatus.CREATED).body(productService.save(productRequestDTO));
	}
	
	@PostMapping("/bulk")
	public List<ProductResponseDTO> createAll(@RequestBody List<ProductRequestDTO> productServiceRequestDTOList)
	{
	return productService.saveAll(productServiceRequestDTOList);
	}
	
	@PutMapping(value = "{id}")
	public Product update(@PathVariable int id, @RequestBody Product product) throws FileErrorException{
	return productService.update(id,product);
	}
}
