package com.bourntec.CityProject.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.bourntec.CityProject.DTO.request.CustomerRequestDTO;
import com.bourntec.CityProject.exception.FileErrorException;
import com.bourntec.CityProject.model.Customer;
import com.bourntec.CityProject.responseDTO.CustomerResponseDTO;
import com.bourntec.CityProject.service.CustomerService;






public class CustomerController {
	@Autowired
	CustomerService customerService;
	
	@GetMapping("/getall")
	public List<CustomerResponseDTO> getAll()
	{
		return customerService.getAll();
	}
	
	@GetMapping("/getName")
	public List<Customer>findByNameAndRecordStatus(String name,String recordStatus)
	{
	return customerService.findByNameAndRecordStatus(name,recordStatus);
	}
	
	@GetMapping("/getAddress")
	public List<Customer>findByAddressAndRecordStatus(String address,String recordStatus)
	{
		return customerService.findByAddressAndRecordStatus(address,recordStatus);
	}
	
	 @GetMapping("/{id}")
	 public Customer get(@PathVariable int id)
	 {
		 return customerService.findById(id); 
	 }
	 
	 @DeleteMapping("/{id}")
	 public void delete(@PathVariable int id)
	 {
		 customerService.deleteById(id);
	 }
	 
	/* @PostMapping("/create-new")
	 public CustomerResponseDTO create(@RequestBody @Valid CustomerRequestDTO customerRequestDTO)
	 {
	 return customerService.save(customerRequestDTO);
	 }*/
	 
		@PostMapping("/create")
		public ResponseEntity create(@RequestBody @Valid CustomerRequestDTO customerRequestDTO)
		{
			return ResponseEntity.status(HttpStatus.CREATED).body(customerService.save(customerRequestDTO));
		}
		
		@PostMapping("/bulk")
		public List<CustomerResponseDTO> createAll(@RequestBody List<CustomerRequestDTO> customerRequestDTOList)
		{
		return customerService.saveAll(customerRequestDTOList);
		}
		
		@PutMapping(value = "{id}")
		public Customer update(@PathVariable int id, @RequestBody Customer customer) throws FileErrorException{
		return customerService.update(id,customer);
		}
}
