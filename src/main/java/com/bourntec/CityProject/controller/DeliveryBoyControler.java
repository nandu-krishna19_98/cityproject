package com.bourntec.CityProject.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.bourntec.CityProject.DTO.request.CustomerRequestDTO;
import com.bourntec.CityProject.DTO.request.DeliveryBoyRequestDTO;
import com.bourntec.CityProject.exception.FileErrorException;
import com.bourntec.CityProject.model.Customer;
import com.bourntec.CityProject.model.DeliveryBoy;
import com.bourntec.CityProject.responseDTO.CustomerResponseDTO;
import com.bourntec.CityProject.responseDTO.DeliveryBoyResponseDTO;
import com.bourntec.CityProject.service.DeliveryBoyService;

public class DeliveryBoyControler {

	@Autowired
	DeliveryBoyService deliveryBoyService;
	
	@GetMapping("/getall")
	public List<DeliveryBoyResponseDTO> getAll()
	{
		return deliveryBoyService.getAll();
	}
	
	@GetMapping("/getName")
	public List<DeliveryBoy>findByNameAndRecordStatus(String name,String recordStatus)
	{
	return deliveryBoyService.findByNameAndRecordStatus(name,recordStatus);
	}
	
	@GetMapping("/{id}")
	 public DeliveryBoy get(@PathVariable int id)
	 {
		 return deliveryBoyService.findById(id);
	 }
	
	 @DeleteMapping("/{id}")
	 public void delete(@PathVariable int id)
	 {
		 deliveryBoyService.deleteById(id);
	 }
	 
	/* @PostMapping("/create-new")
	 public DeliveryBoyResponseDTO create(@RequestBody @Valid DeliveryBoyRequestDTO deliveryBoyRequestDTO)
	 {
	 return deliveryBoyService.save(deliveryBoyRequestDTO);
	 }*/
	 
	 @PostMapping("/create")
		public ResponseEntity create(@RequestBody @Valid DeliveryBoyRequestDTO deliveryBoyRequestDTO)
		{
			return ResponseEntity.status(HttpStatus.CREATED).body(deliveryBoyService.save(deliveryBoyRequestDTO));
		}
	 
	 @PostMapping("/bulk")
		public List<DeliveryBoyResponseDTO> createAll(@RequestBody List<DeliveryBoyRequestDTO> deliveryBoyServiceRequestDTOList)
		{
		return deliveryBoyService.saveAll(deliveryBoyServiceRequestDTOList);
		}
	 
	 @PutMapping(value = "{id}")
		public DeliveryBoy update(@PathVariable int id, @RequestBody DeliveryBoy deliveryBoy) throws FileErrorException{
		return deliveryBoyService.update(id,deliveryBoy);
		}

}
