package com.bourntec.CityProject.repository;


import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.bourntec.CityProject.model.Customer;
import com.bourntec.CityProject.responseDTO.CustomerResponseDTO;


@Repository
public interface CustomerRepository extends JpaRepository<Customer,Integer>,JpaSpecificationExecutor<Customer> {

	public List<Customer>findByNameAndRecordStatus(String name,String recordStatus);
	public List<Customer>findByAddressAndRecordStatus(String address,String recordStatus);
	public List<CustomerResponseDTO> findByRecordStatus(String active);
	Customer findByIdAndRecordStatus(int id,String recordStatus);
	
	@Modifying
	@Transactional
	@Query(value = "update customer set no_of_product_purchased=noOfProductPurchased+:1", nativeQuery = true)
	Integer incrementNoOfProductPurchased();
	
}
