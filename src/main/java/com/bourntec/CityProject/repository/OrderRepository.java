package com.bourntec.CityProject.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.bourntec.CityProject.model.DeliveryBoy;
import com.bourntec.CityProject.model.Order;

import com.bourntec.CityProject.responseDTO.OrderResponseDTO;
@Repository
public interface OrderRepository extends JpaRepository<Order,Integer>,JpaSpecificationExecutor<Order>{
	
	public List<OrderResponseDTO> findByRecordStatus(String recordStatus);
	Order findByIdAndRecordStatus(int id,String recordStatus);
	
	@Modifying
	@Transactional
	@Query(value = "update order set product_id=product_id+:10", nativeQuery = true)
	Integer incrementProductId();
	
}
