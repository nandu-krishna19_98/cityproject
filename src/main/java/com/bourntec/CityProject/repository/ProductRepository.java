package com.bourntec.CityProject.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.bourntec.CityProject.model.Order;
import com.bourntec.CityProject.model.Product;
import com.bourntec.CityProject.responseDTO.ProductResponseDTO;

@Repository
public interface ProductRepository extends JpaRepository<Product,Integer>,JpaSpecificationExecutor<Product>{

	public List<ProductResponseDTO> findByRecordStatus(String recordStatus);
	
	public List<Product>findByNameAndRecordStatus(String name,String recordStatus);

	public List<Product> findByProductNameAndRecordStatus(String productName,String recordStatus);

	public List<Product> findByExpireFieldAndRecordStatus(String expireField,String recordStatus);
	
	Product findByIdAndRecordStatus(int id,String recordStatus);
	
	@Modifying
	@Transactional
	@Query(value = "update product set product_id=product_id+:10", nativeQuery = true)
	Integer incrementProductId();
	
}
