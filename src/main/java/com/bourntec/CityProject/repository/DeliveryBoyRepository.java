package com.bourntec.CityProject.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.bourntec.CityProject.model.Customer;
import com.bourntec.CityProject.model.DeliveryBoy;

import com.bourntec.CityProject.responseDTO.DeliveryBoyResponseDTO;


@Repository
public interface DeliveryBoyRepository extends JpaRepository<DeliveryBoy,Integer>,JpaSpecificationExecutor<DeliveryBoy> {

	public List<DeliveryBoyResponseDTO> findByRecordStatus(String recordStatus);
	public List<DeliveryBoy>findByNameAndRecordStatus(String name,String recordStatus);
	DeliveryBoy findByIdAndRecordStatus(int id,String recordStatus);
	@Modifying
	@Transactional
	@Query(value = "update deliveryBoy set product_id=product_id+:10", nativeQuery = true)
	Integer incrementProductId();
	
}
