package com.bourntec.CityProject.search;

import lombok.Data;

@Data
public class SearchRequest {
	private Operation Operation;
	private String field,value;
	
}
